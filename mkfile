all:V: meowsum meowsum.1

CC=`echo ${CC:-clang}`
meowsum: meowsum.c
	$CC -O3 -mavx -maes -pedantic -Wall -std=c17 -o meowsum meowsum.c

meowsum.1: meowsum.1.scd
	scdoc < meowsum.1.scd > meowsum.1

PREFIX=`echo ${PREFIX:-/usr/local}`
install:V: meowsum meowsum.1
	mkdir -m755 -p $PREFIX/bin
	cp meowsum $PREFIX/bin/meowsum
	cp meowsum.1 /usr/share/man/man1/meowsum.1
